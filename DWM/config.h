/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static const int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int horizpadbar        = 5;        /* horizontal padding for statusbar */
static const int vertpadbar         = 4;        /* vertical padding for statusbar */
static const char *fonts[]          = { "Font Awesome 5 Free Solid:size=11" };
static const char dmenufont[]       = "Fantasque Sans Mono:size=12";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_white[]       = "#eeeeee";
static const char col_cyan[]        = "#8bdedc";
static const char col_orange[]       = "#98971a";
static const char col_black[]       = "#000000";


static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_white, col_black, col_gray2 },
	[SchemeSel]  = { col_black, col_cyan, col_cyan  },

};

/* tagging */
static const char *tags[] = { "", "","", "", "", ""};


static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1,            0,           -1 },
	{ "vlc", 	  NULL,       NULL,       1<<5,         0,           -1 },
	{ "Signal",   NULL,       NULL,       1<<4,         0,           -1 },

};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#include "horizgrid.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
	{ "###",   	  horizgrid},
};

/* key definitions */
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define CONTROLKEY ControlMask

#define TAGKEYS(KEY,TAG) \
	{ CONTROLKEY,                   KEY,      view,           {.ui = 1 << TAG} }, \
	{ ControlMask|ShiftMask,        KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_black, "-nf", col_white, "-sb", col_cyan, "-sf", col_black, NULL };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_r,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
    { ALTKEY|ShiftMask,             XK_0,      togglegaps,     {0} },
    { ALTKEY|ShiftMask|MODKEY,    	XK_0,      defaultgaps,    {0} },
	{ ALTKEY|ShiftMask,             XK_equal,  incrgaps,       {.i = +3 } },
    { ALTKEY|ShiftMask,             XK_minus,  incrgaps,       {.i = -3 } },
    { ALTKEY|ShiftMask|MODKEY,    	XK_h,      incrogaps,      {.i = +3 } },
    { ALTKEY|ShiftMask|MODKEY,    	XK_l,      incrogaps,      {.i = -3 } },
    { ALTKEY|ShiftMask|MODKEY,  	XK_k,      incrigaps,      {.i = +3 } },
    { ALTKEY|ShiftMask|MODKEY,  	XK_j,      incrigaps,      {.i = -3 } },
    { MODKEY,                       XK_y,      incrihgaps,     {.i = +3 } },
    { MODKEY,                       XK_o,      incrihgaps,     {.i = -3 } },
    { MODKEY|ControlMask,           XK_y,      incrivgaps,     {.i = +3 } },
    { MODKEY|ControlMask,           XK_o,      incrivgaps,     {.i = -3 } },
    { MODKEY|Mod1Mask,              XK_y,      incrohgaps,     {.i = +3 } },
    { ALTKEY|MODKEY,              	XK_o,      incrohgaps,     {.i = -3 } },
    { MODKEY|ShiftMask,             XK_y,      incrovgaps,     {.i = +3 } },
    { MODKEY|ShiftMask,             XK_o,      incrovgaps,     {.i = -3 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -3 } },
//	{ MODKEY|ShiftMask,             XK_j,      inplacerotate,  {.i = +1} },
//	{ MODKEY|ShiftMask,             XK_k,      inplacerotate,  {.i = -1} },
	{ MODKEY|ShiftMask,             XK_j,      inplacerotate,  {.i = +2} },
	{ MODKEY|ShiftMask,             XK_k,      inplacerotate,  {.i = -2} },
	{ MODKEY|ShiftMask,             XK_h,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_l,      incnmaster,     {.i = -1 } },
	{ ALTKEY|ShiftMask,             XK_l,      setmfact,       {.f = -0.02} },
	{ ALTKEY|ShiftMask,             XK_h,      setmfact,       {.f = +0.02} },
	{ MODKEY|CONTROLKEY,            XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_h,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|CONTROLKEY,            XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

